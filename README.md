# Answers to Interview Questions:


### Q1. What is the difference between currentTarget and target when talking about events?

Ans: Whenever a JavaScript event is triggered, it runs the event handlers attached to it first and then runs the event handlers attached to its parent(s) and/or ancestor(s). 
<br>
Taking this into account, the target is always the element where the event occurred and value for the same never changes. On the other hand, currentTarget is the `this` value; the one that has a currently running handler on it. 

<br>
For the following code, Everytime we click on the `<p>` tag, Inside the click handler of `div#demo`, the currentTarget will be the `<div>` and the target will be `<p>`. 

```markup
<div id="demo">
    <p>Some text</p>
</div>

```

```javascript
//JS Code
document.getElementById('demo').addEventListener('click', function (event){ console.log(event.currentTarget); console.log(event.target); } )

```


<br>
Also, the target may or may not have a handler attached to it. 

---

### Q2. What is the benefit to using a library like React/Vue/Angular vs using plain old JavaScript?
Ans:
1. We get the benifits of using the latest JavaScript/ECMAScript features
2. Most of the Browser Inconsitancies are automatically taken care by them
3. A lot of libraries are becoming more and more declarative, instead of imperative 
4. Sometimes, these libraries can also be used to enforce better code and standards
5. Eases the work of developers, Adding a feature sometimes can be as simple as just importing a module

---

### Q3. Where can JavaScript be run?
Ans: The question is now where it cannot be run. We can run it in Browsers, TVs, Mobile Phones, Smart Watches, Toys, etc.
(May be the next target could be cats and dogs communicating in JS?)
<br />
I am currently travelling in Bangalore, India and Last week I attended a JavaScript Conference, JSFOO. 
One talk that really inspired me was by George Mandis. He spoke about making midi controllers/keyboards communicate with Tiny Computers and JavaScript
<br />
Youtube Link:https://www.youtube.com/watch?v=R0-XLrr8icY
<br>
Adding to above, I was exited to see WebVR being developed so extensively along with ARCore and ARKit. 

---

### Q4. What is ES6?
Ans: EcmaScript is a scripting language specification on which languages like JavaScript & ActionScript are based upon. <br>
ES6 is the 6th edition of this specification released in 2015. It introduced many new features like Classes, Modules, Arrow Functions, etc.<br>
To check the status of the ES6 features implimented and supported by browsers, Follow [This Chart](http://kangax.github.io/compat-table/es6/).

---

### Q5. Can you give an example of prototypal inheritance?
Ans: Prototypes are similar to classes, but classes are more or less like blueprints of objects; whereas Objects are real implementation. 
In JavaScript, everything is an object and a child of an `Object` Prototype. 
There are mainly 2 ways in which an Object can inherit the properties of its parent object.

<br>
One of the ways is to create a prototype object as shown below. 

```javascript

// ES6, please use babel to compile if
// you are using in an ES5 environment

const phones ={
  init (name,screenSize){
    this.name = name;
    this.screenSize = screenSize;
  },
  getName(){
    console.log(this.name)
  }
} // This is a prototype of iPhone and galaxy variables

// Creating Objects from the "phones" prototype
let iPhone = Object.create(phones);
let galaxy = Object.create(phones);

iPhone.init('iPhone',4.7);
galaxy.init('galaxy',5);

phones.getName = function (){
  // Overriding this, just to prove that
  // Object.create() does not create a copy,
  // instead creates a prototype which is
  // shared between both the variables iPhone and
  console.log(`You assigned a phone by the name ${this.name} with a ${this.screenSize} inch screen`);
}


// Now, since getName() function is
// not present in iPhone and galaxy Objects,
// JS engine searches in protoytype of these objects,
// which is : `phones` Object
iPhone.getName()
galaxy.getName()


```

<br>
Another method could be creating a constructor function. This constructor function acts as a blueprint or prototype of the object. 

---

Thank You, 
Prashant Sani